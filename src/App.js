import React from 'react';
import logo from './gitlab.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI
        </a>
      </header>

      <div className="participants">
        <h3>
          Add your name to the list to demonstrate you have completed the course.
        </h3>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Location</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Jesse</td>
              <td>@jpadillaa</td>
              <td>Colombia CO</td>
              <td>Thank you for taking this course.</td>
            </tr> </tbody>
          </table>
        </div>


    </div>
  );
}

export default App;
